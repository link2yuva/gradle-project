package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.WebDriverEventListener;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCName;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLName;
	@FindBy(how=How.NAME, using="submitButton") WebElement eleCreateLead;
	@FindBy(how=How.ID, using="createLeadForm_dataSourceId") WebElement eleSource;
	@FindBy(how=How.XPATH, using="(//input[@id='createLeadForm_parentPartyId']//following::img)[1]") WebElement eleParentAccount;
	@FindBy(how=How.ID, using="createLeadForm_marketingCampaignId") WebElement eleMarketingCampaign;
	@FindBy(how=How.ID, using="createLeadForm_firstNameLocal") WebElement eleFNameLocal;
	@FindBy(how=How.ID, using="createLeadForm_lastNameLocal") WebElement eleLNameLocal;
	@FindBy(how=How.ID, using="createLeadForm_personalTitle") WebElement eleSalutation;
	@FindBy(how=How.ID, using="createLeadForm_generalProfTitle") WebElement eleTitle;
	@FindBy(how=How.ID, using="createLeadForm_departmentName") WebElement eleDepartment;
	@FindBy(how=How.ID, using="createLeadForm_annualRevenue") WebElement eleAnnualRevenue;
	@FindBy(how=How.ID, using="createLeadForm_currencyUomId") WebElement eleCurrency;
	@FindBy(how=How.ID, using="createLeadForm_industryEnumId") WebElement eleIndustry;
	@FindBy(how=How.ID, using="createLeadForm_numberEmployees") WebElement eleNumOfEmployees;
	@FindBy(how=How.ID, using="createLeadForm_ownershipEnumId") WebElement eleOwnership;
	@FindBy(how=How.ID, using="createLeadForm_sicCode") WebElement eleSICcode;
	@FindBy(how=How.ID, using="createLeadForm_tickerSymbol") WebElement eleTicker;

	@Given("enter Company Name as (.*)")
	public CreateLeadPage enterCompanyName(String data)
	{
		type(eleCName, data);
		return this;
	}
	@Given("enter First Name as (.*)")
	public CreateLeadPage enterFirstName(String data)
	{
		type(eleFName, data);
		return this;
	}
	@Given("enter Last Name as (.*)")
	public CreateLeadPage enterLastName(String data)
	{
		type(eleLName, data);
		return this;
	}

	public CreateLeadPage selectSource(String data) {
		selectDropDownUsingText(eleSource, data);
		return this;
	}
	
	public FindAccountsWindow clickParentAccount()
	{
		clickWithNoSnap(eleParentAccount);
		return new FindAccountsWindow();
	}
	
	public CreateLeadPage switchToCreadLeadPage()
	{
		switchToWindow(0);
		return this;
	}
	
	@When("click CreateLead button")
	public ViewLeadPage clickCreateLeadbutton()
	{
		click(eleCreateLead);
		return new ViewLeadPage();
	}
	
	public CreateLeadPage selectMarketingCampaign(String data) {
		selectDropDownUsingValue(eleMarketingCampaign, data);
		return this;
	}
	
	public CreateLeadPage enterFirstNameLocal(String data)
	{
		type(eleFNameLocal, data);
		return this;
	}
	
	public CreateLeadPage enterLastNameLocal(String data)
	{
		type(eleLNameLocal, data);
		return this;
	}

	public CreateLeadPage enterSalutation(String data)
	{
		type(eleSalutation, data);
		return this;
	}
	
	public CreateLeadPage enterTitle(String data)
	{
		type(eleTitle, data);
		return this;
	}
	
	public CreateLeadPage enterDepartment(String data)
	{
		type(eleDepartment, data);
		return this;
	}
	
	public CreateLeadPage enterAnnualRevenue(String data)
	{
		type(eleAnnualRevenue, data);
		return this;
	}
	
	public CreateLeadPage selectPreferredCurrency(String data)
	{
		selectDropDownUsingText(eleCurrency, data);
		return this;
	}
	
	public CreateLeadPage selectIndustry(String data)
	{
		selectDropDownUsingText(eleIndustry, data);
		return this;
	}
	
	public CreateLeadPage enterNumberOfEmployees(String data)
	{
		type(eleNumOfEmployees, data);
		return this;
	}
	
	public CreateLeadPage selecteleOwnership(String data)
	{
		selectDropDownUsingText(eleOwnership, data);
		return this;
	}
	
	public CreateLeadPage enterSICCode(String data)
	{
		type(eleSICcode, data);
		return this;
	}
	
	public CreateLeadPage enterTicker(String data)
	{
		type(eleTicker, data);
		return this;
	}
	
	
}







