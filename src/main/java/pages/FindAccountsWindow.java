package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindAccountsWindow extends ProjectMethods{
	
	public FindAccountsWindow() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//a[@class='linktext']") WebElement eleAccountId;
	
	public FindAccountsWindow switchToFindParentAccountWindow()
	{
		switchToWindow(1);
		return this;
	}
	
	public CreateLeadPage clickAccountId()
	{
		clickWithNoSnap(eleAccountId);
		return new CreateLeadPage();
	}
	
	
}
