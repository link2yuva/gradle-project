package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class MyHomePage extends ProjectMethods{

	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT, using="Leads") WebElement eleLeadLink;
	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement eleCreateLeadLink;
	
	public MyLeadsPage clickLead()
	{
		click(eleLeadLink);
		return new MyLeadsPage();
		
	}	
	@Given("click CreateLead link")
	public CreateLeadPage createLead()
	{
		click(eleCreateLeadLink);
		return new CreateLeadPage();
	}
}







