package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.WebDriverEventListener;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[last()]") WebElement eleFirstName;
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement eleFindLeads;
	@FindBy(how=How.XPATH, using="(//a[@class='linktext'])[4]") WebElement eleLeadId;
	@FindBy(how=How.XPATH, using="//input[@name='phoneNumber']") WebElement elePhoneNumber;
	@FindBy(how=How.XPATH, using="//span[text()='Phone']") WebElement elePhone;
	@FindBy(how=How.XPATH, using="//span[text()='Email']") WebElement eleEmail;
	@FindBy(how=How.XPATH, using="//input[@name='emailAddress']") WebElement eleEmailAddress;
	
	public FindLeadsPage enterFirstName(String data)
	{
		type(eleFirstName, data);
		return this;
	}
	
	public FindLeadsPage clickFindLeads()
	{
		click(eleFindLeads);
		return this;
	}
	
	public ViewLeadPage clickLeadId()
	{
		click(eleLeadId);
		return new ViewLeadPage();
	}
	
	public FindLeadsPage enterPhoneNumber(String data)
	{
		type(elePhoneNumber, data);
		return this;
	}
	
	public FindLeadsPage clickPhone()
	{
		click(elePhone);
		return this;
	}
	
	public FindLeadsPage clickEmail()
	{
		click(eleEmail);
		return this;
	}
	
	public FindLeadsPage enterEmailAddress(String data)
	{
		type(eleEmailAddress, data);
		return this;
	}
}







