package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDescription ="Create Lead in leaftaps";
		testNodes = "Leads";
		authors ="Yuvaraj";
		category = "smoke";
		dataSheetName="TC001_CreateLead";
	}

	@Test(dataProvider="fetchData")
	public void CreateLead(String uName, String pwd, String cName, String fName, String lName, String source, String marketingMampaign,
			String fNameLocal, String lNameLocal, String salutation, String title, String department, String annualRevenue, String preferredCurrency,
			String industry, String NoOfEmployees, String Ownership, String sicCode, String ticker, String description, String impNote,
			String countryCode, String areaCode, String phNumber, String extnNum, String askForName, String eMailId, String webUrl,
			String toName, String AttnName, String address1, String address2, String city, String country, String state, String pinCode, 
			String pinCodeExt)
	{
		new LoginPage()
		.enterUsername(uName)
		.enterPassword(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickCreateLead()
		.enterCompanyName(cName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickParentAccount()
		.switchToFindParentAccountWindow()
		.clickAccountId()
		.switchToCreadLeadPage()
		.selectSource(source)
		.selectMarketingCampaign(marketingMampaign)
		.clickCreateLeadbutton();			
	}

}
