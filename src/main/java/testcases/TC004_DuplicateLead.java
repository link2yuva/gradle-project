package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_DuplicateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_DuplicateLead";
		testDescription ="Duplicate Lead in leaftaps";
		testNodes = "Leads";
		authors ="Yuvaraj";
		category = "smoke";
		dataSheetName="TC004_DuplicateLead";
	}
	
	@Test(dataProvider="fetchData")
	public void CreateLead(String uName, String pwd, String PhNumber)
	{
		new LoginPage()
		.enterUsername(uName)
		.enterPassword(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickFindLeads()
		.clickPhone()
		.enterPhoneNumber(PhNumber)
		.clickFindLeads()
		.clickLeadId()
		.clickDeleteLead();
	}

}
