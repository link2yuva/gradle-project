Feature: Create Lead for LeafTaps Application

@smoke
Scenario Outline: TC001_CreateLead Positive flow
Given enter the username as DemoSalesManager
And enter the password as crmsfa
And click login button
And click CRMSFA link 
And click CreateLead link
And enter Company Name as <CompanyName> 
And enter First Name as <FirstName>
And enter Last Name as <LastName>
When click CreateLead button
#Then Verify CreateLead is success
Examples: 
|CompanyName|FirstName|LastName|
|Infosys|Yuvaraj|Dhanasekaran|
|Infy|Yuvaraj|D|

@smoke
Scenario: TC002_CreateLead Positive flow
Given  enter the username as DemoCSR
And enter the password as crmsfa
And click login button
And click CRMSFA link
And click CreateLead link
And enter Company Name as Infosys
And enter First Name as Yuvaraj
And enter Last Name as Dhanasekaran
When click CreateLead button
#Then Verify CreateLead is success

Scenario: TC003_CreateLead Negative flow
Given  enter the username as DemoCSR
And enter the password as crmsfa
And click login button
And click CRMSFA link
And click CreateLead link
And enter Company Name as Infosys
And enter First Name as Yuvaraj
When click CreateLead button
#But Verify CreateLead is Failed

