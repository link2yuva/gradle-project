/*package stepDefinition;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TC001_CreateLead {

	ChromeDriver driver;

	@Given("open the browser")
	public void openTheBrowser() {
		// Write code here that turns the phrase above into concrete actions
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();}

	@Given("enter the url")
	public void enterTheUrl() {
		// Write code here that turns the phrase above into concrete actions
		driver.get("http://leaftaps.com/opentaps/control/main");
	}

	@Given("enter the username as (.*)")
	public void enterTheUsernameAsDemoSalesManager(String userName) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("username").sendKeys(userName);	
	}

	@Given("enter the password as crmsfa")
	public void enterThePasswordAsCrmsfa() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("password").sendKeys("crmsfa");

	}

	@Given("click login button")
	public void clickLoginButton() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Given("click CRM/SFA link")
	public void clickCRMSFALink() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("click CreateLead link")
	public void clickCreateLeadLink() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Create Lead").click();
	}

	@Given("enter Company Name as (.*)")
	public void enterCompanyName(String cName) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_companyName").sendKeys(cName);	}

	@Given("enter First Name as (.*)")
	public void enterFirstName(String fName) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);	}

	@Given("enter Last Name as (.*)")
	public void enterLastName(String lName) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	}

	@When("click CreateLead button")
	public void clickCreateLeadButton() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByClassName("smallSubmit").click();	}

	@Then("Verify CreateLead is success")
	public void verifyCreateLeadIsSuccess() {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("Create Lead Success");	
		}

	@But("Verify CreateLead is Failed")
	public void VerifyCreateLeadIsFailed() {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("Create Lead Failed");	
		}


}
*/